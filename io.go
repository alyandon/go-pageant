// +build !windows

package pageant

import (
	"log"

	"golang.org/x/crypto/ssh/agent"
)

// New returns new ssh-agent instance (see http://golang.org/x/crypto/ssh/agent)
func New() agent.Agent {
	log.Fatal("Pageant not support on non-windows platforms")
	return nil
}
